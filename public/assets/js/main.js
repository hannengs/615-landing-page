/************* Main Js File ************************
    Template Name: gauto
    Author: Themescare
    Version: 1.0
    Copyright 2019
*************************************************************/


/*------------------------------------------------------------------------------------
    
JS INDEX
=============

01 - Main Slider JS
02 - Select JS
03 - Clockpicker JS
04 - Service Slider JS
05 - Testimonial Slider JS
06 - Responsive Menu JS
07 - Back To Top




-------------------------------------------------------------------------------------*/
$(window).on('load',function() { 
	/* 
		=================================================================
		05 - Brand Slider JS
		=================================================================	
		*/


		$(".brand-slide").owlCarousel({
			autoplay: true,
			loop: true,
			margin: 25,
			touchDrag: true,
			mouseDrag: true,
			items: 3,
			nav: true,
			dots: true,
			autoplayTimeout: 6000,
			autoplaySpeed: 1200,
			navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
			responsive: {
				0: {
					items: 1
				},
				480: {
					items: 1
				},
				600: {
					items: 1
				},
				1000: {
					items: 3
				},
				1200: {
					items: 3
				}
			}
		});

		// $(".brand-slide2").owlCarousel({
		// 	autoplay: true,
		// 	loop: true,
		// 	center:true,
		// 	margin: 10,
		// 	touchDrag: true,
		// 	mouseDrag: true,
		// 	items: 3,
		// 	nav: true,
		// 	dots: true,
		// 	autoplayTimeout: 6000,
		// 	autoplaySpeed: 1200,
		// 	navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
		// 	responsive: {
		// 		0: {
		// 			items: 2
		// 		},
		// 		480: {
		// 			items: 2
		// 		},
		// 		600: {
		// 			items: 3
		// 		},
		// 		1000: {
		// 			items: 3
		// 		},
		// 		1200: {
		// 			items: 3
		// 		}
		// 	}
		// });


		// ALL BRANDS
		const scrollall = document.getElementById('all-tab');
		scrollall.addEventListener('click', () => {
		    const appointmentFullElement = document.getElementById('offerTabContent');
		    
		    if (appointmentFullElement) {
		        appointmentFullElement.scrollIntoView({ behavior: 'smooth' });
		    }
		});

		// BMW
		const scrollbmw = document.getElementById('bmw-tab');
		scrollbmw.addEventListener('click', () => {
		    const appointmentFullElement = document.getElementById('offerTabContent');
		    
		    if (appointmentFullElement) {
		        appointmentFullElement.scrollIntoView({ behavior: 'smooth' });
		    }
		});

		// PORSCHE
		const scrollporsche = document.getElementById('porsche-tab');
		scrollporsche.addEventListener('click', () => {
		    const appointmentFullElement = document.getElementById('offerTabContent');
		    
		    if (appointmentFullElement) {
		        appointmentFullElement.scrollIntoView({ behavior: 'smooth' });
		    }
		});

		// CADILLAC
		const scrollcadillac = document.getElementById('cadillac-tab');
		scrollcadillac.addEventListener('click', () => {
		    const appointmentFullElement = document.getElementById('offerTabContent');
		    
		    if (appointmentFullElement) {
		        appointmentFullElement.scrollIntoView({ behavior: 'smooth' });
		    }
		});

		// HYUNDAI
		const scrollhyundai = document.getElementById('hyundai-tab');
		scrollhyundai.addEventListener('click', () => {
		    const appointmentFullElement = document.getElementById('offerTabContent');
		    
		    if (appointmentFullElement) {
		        appointmentFullElement.scrollIntoView({ behavior: 'smooth' });
		    }
		});

		// KIA
		const scrollkia = document.getElementById('kia-tab');
		scrollkia.addEventListener('click', () => {
		    const appointmentFullElement = document.getElementById('offerTabContent');
		    
		    if (appointmentFullElement) {
		        appointmentFullElement.scrollIntoView({ behavior: 'smooth' });
		    }
		});

		// NISSAN
		const scrollnissan = document.getElementById('nissan-tab');
		scrollnissan.addEventListener('click', () => {
		    const appointmentFullElement = document.getElementById('offerTabContent');
		    
		    if (appointmentFullElement) {
		        appointmentFullElement.scrollIntoView({ behavior: 'smooth' });
		    }
		});

		// FORD
		const scrollford = document.getElementById('ford-tab');
		scrollford.addEventListener('click', () => {
		    const appointmentFullElement = document.getElementById('offerTabContent');
		    
		    if (appointmentFullElement) {
		        appointmentFullElement.scrollIntoView({ behavior: 'smooth' });
		    }
		});

		// TOYOTA
		const scrolltoyota = document.getElementById('toyota-tab');
		scrolltoyota.addEventListener('click', () => {
		    const appointmentFullElement = document.getElementById('offerTabContent');
		    
		    if (appointmentFullElement) {
		        appointmentFullElement.scrollIntoView({ behavior: 'smooth' });
		    }
		});

		// DODGE
		const scrolldodge = document.getElementById('dodge-tab');
		scrolldodge.addEventListener('click', () => {
		    const appointmentFullElement = document.getElementById('offerTabContent');
		    
		    if (appointmentFullElement) {
		        appointmentFullElement.scrollIntoView({ behavior: 'smooth' });
		    }
		});

		// RANGE
		const scrollrange = document.getElementById('range-tab');
		scrollrange.addEventListener('click', () => {
		    const appointmentFullElement = document.getElementById('offerTabContent');
		    
		    if (appointmentFullElement) {
		        appointmentFullElement.scrollIntoView({ behavior: 'smooth' });
		    }
		});

		// MERCEDES
		const scrollmercedes = document.getElementById('mercedes-tab');
		scrollmercedes.addEventListener('click', () => {
		    const appointmentFullElement = document.getElementById('offerTabContent');
		    
		    if (appointmentFullElement) {
		        appointmentFullElement.scrollIntoView({ behavior: 'smooth'});
		    }
		});



		$(".brand-slidecad").owlCarousel({
			autoplay: true,
			loop: true,
			margin: 25,
			touchDrag: true,
			mouseDrag: true,
			items: 3,
			nav: true,
			dots: true,
			autoplayTimeout: 6000,
			autoplaySpeed: 1200,
			navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
			responsive: {
				0: {
					items: 1
				},
				480: {
					items: 1
				},
				600: {
					items: 1
				},
				1000: {
					items: 3
				},
				1200: {
					items: 3
				}
			}
		});
});

(function ($) {
	"use strict";

	jQuery(document).ready(function ($) {

		AOS.init();

		$("gauto-responsive-menu").find($(".slicknav_menu:eq(0)").css("display","none"));


		/* 
		=================================================================
		01 - Main Slider JS
		=================================================================	
		*/

		$(".gauto-slide").owlCarousel({
			animateOut: 'fadeOutLeft',
			animateIn: 'fadeIn',
			items: 2,
			nav: true,
			dots: false,
			autoplayTimeout: 9000,
			autoplaySpeed: 5000,
			autoplay: true,
			loop: true,
			navText: ["<img src='assets/img/prev-1.png'>", "<img src='assets/img/next-1.png'>"],
			mouseDrag: true,
			touchDrag: true,
			responsive: {
				0: {
					items: 1
				},
				480: {
					items: 1
				},
				600: {
					items: 1
				},
				750: {
					items: 1
				},
				1000: {
					items: 1
				},
				1200: {
					items: 1
				}
			}
		});

		

		$(".gauto-slide").on("translate.owl.carousel", function () {
			$(".gauto-main-slide h2").removeClass("animated fadeInUp").css("opacity", "0");
			$(".gauto-main-slide p").removeClass("animated fadeInDown").css("opacity", "0");
			$(".gauto-main-slide .gauto-btn").removeClass("animated fadeInDown").css("opacity", "0");
		});
		$(".gauto-slide").on("translated.owl.carousel", function () {
			$(".gauto-main-slide h2").addClass("animated fadeInUp").css("opacity", "1");
			$(".gauto-main-slide p").addClass("animated fadeInDown").css("opacity", "1");
			$(".gauto-main-slide .gauto-btn").addClass("animated fadeInDown").css("opacity", "1");
		});

		/* 
		=================================================================
		02 - Select JS
		=================================================================	
		*/

		$('select').niceSelect();

		/* 
		=================================================================
		03 - Clockpicker JS
		=================================================================	
		*/

		$('.clockpicker').clockpicker();


		/* 
		=================================================================
		04 - Service Slider JS
		=================================================================	
		*/

		$(".service-slider").owlCarousel({
			autoplay: true,
			loop: true,
			margin: 25,
			touchDrag: true,
			mouseDrag: true,
			items: 1,
			nav: false,
			dots: true,
			autoplayTimeout: 6000,
			autoplaySpeed: 1200,
			navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
			responsive: {
				0: {
					items: 1
				},
				480: {
					items: 1
				},
				600: {
					items: 2
				},
				1000: {
					items: 3
				},
				1200: {
					items: 3
				}
			}
		});


		/* 
		=================================================================
		05 - Testimonial Slider JS
		=================================================================	
		*/

		// $(".testimonial-slider").owlCarousel({
		// 	autoplay: true,
		// 	loop: true,
		// 	margin: 25,
		// 	touchDrag: true,
		// 	mouseDrag: true,
		// 	items: 1,
		// 	nav: false,
		// 	dots: true,
		// 	autoplayTimeout: 6000,
		// 	autoplaySpeed: 1200,
		// 	navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
		// 	responsive: {
		// 		0: {
		// 			items: 1
		// 		},
		// 		480: {
		// 			items: 1
		// 		},
		// 		600: {
		// 			items: 1
		// 		},
		// 		1000: {
		// 			items: 3
		// 		},
		// 		1200: {
		// 			items: 3
		// 		}
		// 	}
		// });

		


		/* 
		=================================================================
		06 - Responsive Menu JS
		=================================================================	
		*/
		$("ul#gauto_navigation").slicknav({
			prependTo: ".gauto-responsive-menu"
		});


		/* 
		=================================================================
		07 - Back To Top
		=================================================================	
		*/
		if ($("body").length) {
			var btnUp = $('<div/>', {
				'class': 'btntoTop'
			});
			btnUp.appendTo('body');
			$(document).on('click', '.btntoTop', function () {
				$('html, body').animate({
					scrollTop: 0
				}, 700);
			});
			$(window).on('scroll', function () {
				if ($(this).scrollTop() > 200) $('.btntoTop').addClass('active');
				else $('.btntoTop').removeClass('active');
			});
		}


	});


}(jQuery));

function sendContact() {
    Email.send({
            SecureToken: "bf50b5d2-1d83-4124-83ae-188900851b6a",
            To: "hannahendell@gmail.com",
            From: document.getElementById('contactemail').value,
            Subject: "Question from" + document.getElementById('contactname').value,
            Body: document.getElementById('contactquestion').value
        }).then( 
            message => alert("Mail sent successfully")
        );
}
